def vowel_swapper(string):
    # ==============
    # a becomes 4
    # e becomes 3
    # i becomes !
    # o becomes ooo
    # u becomes |_|

    str1 = string.replace("a", "4").replace("e", "3").replace(
        "i", "!").replace("o", "ooo").replace("u", "|_|")
    return str1
    # ==============


# Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("aA eE iI oO uU"))
# Should print "H3llooo Wooorld" to the console
print(vowel_swapper("Hello World"))
# Should print "3v3ryth!ng's 4v4!l4bl3" to the console
print(vowel_swapper("Everything's Available"))
