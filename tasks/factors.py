def factors(number):
    # ==============
    factors = []
    for n in range(1, number):
        if not n == 1 or n == number:
            if number % n == 0:
                factors.append(n)
    return factors

    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “[]” (an empty list) to the console
