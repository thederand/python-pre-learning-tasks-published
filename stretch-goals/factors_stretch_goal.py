def factors(number):
    # ==============
    factors = []
    for n in range(1, number):
        if not n == 1 or n == number:
            if number % n == 0:
                factors.append(n)
    if (len(factors) == 0):
        return f"{number} is a prime number"
    return factors

    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
