def vowel_swapper(string):
    # ==============
    """
    a -> /\
    e -> 3
    i -> !
    o -> 000
    u -> \/
    """
    vowel = {
        "a": "/\\",
        "e": "3",
        "i": "!",
        "o": "000",
        "u": "\/"
    }

    arrA = list(string)

    for v in vowel:
        count = 0
        for i, c in enumerate(arrA):
            if c.lower() == v:
                count += 1
                if count == 2:
                    arrA[i] = vowel[v]
    return "".join(arrA)

    # ==============


# Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("aAa eEe iIi oOo uUu"))
# Should print "Hello Wooorld" to the console
print(vowel_swapper("Hello World"))
# Should print "Ev3rything's Av/\!lable" to the console
print(vowel_swapper("Everything's Available"))
